add_formatter_test(clang-format-3.8 pass format.c pass)
add_formatter_test(clang-format-3.8 format format.c format)
add_formatter_test(clang-format-3.8 no-config-file format.c fail)
