foreach(v
    3.8
    6.0
    )
  set(ver ${v})
  configure_file(format.clang-format.in format.clang-format-${v} @ONLY)
  configure_file(clang-format.yaml.in clang-format-${v}.yaml.in COPYONLY)
  add_formatter(clang-format-${v} reformat clang-format-${v})
  set_property(GLOBAL PROPERTY "formatter_clang-format-${v}_exclude_from_all" "1")
endforeach()
add_formatter(clang-format reformat clang-format)
set_property(GLOBAL PROPERTY "formatter_clang-format_exclude_from_all" "1")
add_formatter(clang-format-kwsys reformat clang-format-6.0)
set_property(GLOBAL PROPERTY "formatter_clang-format-kwsys_exclude_from_all" "1")
add_formatter(uncrustify reformat uncrustify)
set_property(GLOBAL PROPERTY "formatter_uncrustify_exclude_from_all" "1")
